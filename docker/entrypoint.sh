#!/bin/bash -l
set -e
if [ "$#" -eq 0 ]; then
  echo "Use 'testpublisher' or 'testsubscriber' (just pass the whole command line as argument to docker run)"
  echo ""
  testpublisher --help
  echo ""
  testsubscriber --help
  true
else
  exec "$@"
fi
