"""Common fixtures"""
from typing import Optional, Any
import asyncio
import uuid
from dataclasses import dataclass, field
from pathlib import Path
import logging
import platform
import random

import toml
import pytest
import pytest_asyncio
from libadvian.testhelpers import nice_tmpdir  # pylint: disable=W0611
from datastreamcorelib.logging import init_logging
from datastreamcorelib.pubsub import Subscription
from datastreamcorelib.datamessage import PubSubDataMessage

from datastreamservicelib.service import SimpleService
from datastreamservicelib.console import CollectingSubscriber
from datastreamservicelib.reqrep import FullService, REQMixin
from datastreamservicelib.compat import asyncio_eventloop_check_policy, asyncio_eventloop_get

# pylint: disable=W0621
init_logging(logging.DEBUG)
LOGGER = logging.getLogger(__name__)
asyncio_eventloop_check_policy()


@pytest.fixture
def event_loop():  # type: ignore
    """override pytest-asyncio default eventloop"""
    loop = asyncio_eventloop_get()
    LOGGER.debug("Yielding {}".format(loop))
    yield loop
    loop.close()


class CBTestException(RuntimeError):
    """Just for testing exception raising in callbacks"""


@dataclass  # pylint: disable=R0901
class REPlier(FullService):  # pylint: disable=R0901
    """Full service with some basic methods to call"""

    async def echo(self, *args: Any) -> Any:
        """return the args"""
        _ = self
        await asyncio.sleep(0.01)
        return args

    def true(self) -> bool:
        """returns true, can be used to test invalid arg numbers etc"""
        _ = self
        return True

    def fail(self) -> None:
        """Raises error"""
        _ = self
        raise RuntimeError("you asked for failure")


@dataclass  # pylint: disable=R0901
class Subscriber(REQMixin, CollectingSubscriber):
    """Subscriber service that"""

    publisher: Optional[SimpleService] = field(default=None)

    def subscribe_default(self, topic: bytes) -> Subscription:
        """Create a subscription to the default publisher"""
        assert isinstance(self.publisher, SimpleService)
        sub = Subscription(
            self.publisher.config["zmq"]["pub_sockets"], topic, self.success_callback, decoder_class=PubSubDataMessage
        )
        LOGGER.debug("Subscribing to {!r}".format(sub.socketuris))
        self.messages_by_sub[sub.trackingid] = []
        self.psmgr.subscribe(sub)
        return sub


async def wait_for_hbtask(serv: SimpleService) -> None:
    """Wait for the heartbeat task to be created"""
    # pylint: disable=W0212
    while "HEARTBEAT" not in serv._tasks:
        await asyncio.sleep(0.01)


@pytest_asyncio.fixture
async def publisher(nice_tmpdir, event_loop):  # type: ignore
    """Create a publisher"""
    workdir = Path(nice_tmpdir)
    assert workdir.exists()
    configpath = workdir / "publisher.toml"
    random_str = str(uuid.uuid4())
    pub_sock_path = "ipc://" + str(Path(nice_tmpdir) / (random_str + "_pub.sock"))
    rep_sock_path = "ipc://" + str(Path(nice_tmpdir) / (random_str + "_rep.sock"))
    if platform.system() == "Windows":
        pub_sock_path = f"tcp://127.0.0.1:{random.randint(1337, 65000)}"  # nosec
        rep_sock_path = f"tcp://127.0.0.1:{random.randint(1337, 65000)}"  # nosec
    with open(configpath, "wt", encoding="utf-8") as fpntr:
        toml.dump({"zmq": {"pub_sockets": [pub_sock_path], "rep_sockets": [rep_sock_path]}}, fpntr)
    # Create service instance and make it run
    serv = REPlier(configpath)
    task = event_loop.create_task(serv.run())
    await asyncio.wait_for(wait_for_hbtask(serv), timeout=0.1)
    assert serv.psmgr.default_pub_socket
    yield serv
    serv.quit()
    await task
    SimpleService.clear_exit_alarm()


@pytest_asyncio.fixture
async def subscriber(nice_tmpdir, publisher, event_loop):  # type: ignore
    """Create subscriber"""
    configpath = Path(str(publisher.configpath).replace("publisher.toml", "subscriber.toml"))
    pub_sock_path = "ipc://" + str(Path(nice_tmpdir) / (str(uuid.uuid4()) + ".sock"))
    if platform.system() == "Windows":
        pub_sock_path = f"tcp://127.0.0.1:{random.randint(1337, 65000)}"  # nosec
    with open(configpath, "wt", encoding="utf-8") as fpntr:
        toml.dump(
            {"zmq": {"pub_sockets": [pub_sock_path], "sub_sockets": [publisher.config["zmq"]["pub_sockets"][0]]}}, fpntr
        )
    serv = Subscriber(configpath, str(uuid.uuid4()).encode("ascii"), publisher=publisher)
    task = event_loop.create_task(serv.run())
    await asyncio.wait_for(wait_for_hbtask(serv), timeout=0.1)
    yield serv
    serv.quit()
    await task
    SimpleService.clear_exit_alarm()
