"""Test that exceptions cause service to shutdown cleanly"""
import logging
import asyncio
import datetime
import time
import threading
import sys
import tempfile
import uuid
import signal as posixsignal
from pathlib import Path
from dataclasses import dataclass, field

import platform
import random

import pytest
import click
import toml
from libadvian.logging import init_logging
from libadvian.testhelpers import nice_tmpdir  # pylint: disable=W0611

from datastreamservicelib.service import SimpleService, EXCEPTION_EXITCODE


LOGGER = logging.getLogger(__name__)


@dataclass
class BrokenService(SimpleService):
    """Badly broken service"""

    daemonized_uncoop: bool = field(default=False)

    def reload(self) -> None:
        """Reload"""
        asyncio.get_event_loop().create_task(self.printer_task())
        thread = threading.Thread(target=self.coop_printer_thread)
        thread.start()
        thread = threading.Thread(target=self.uncoop_printer_thread, daemon=self.daemonized_uncoop)
        thread.start()
        asyncio.get_event_loop().create_task(self.exceptioning_task())

    async def exceptioning_task(self) -> None:
        """task throwing an unhandled exception"""
        while True:
            await asyncio.sleep(0.5)
            # This should be LOGGING but we're making a very badly broken example here...
            print("(etask) The time is: {}Z".format(datetime.datetime.utcnow().isoformat()))
            raise RuntimeError("Puking my guts out")

    def uncoop_printer_thread(self) -> None:
        """Also print stuff, but from a thread"""
        while True:
            # This should be LOGGING but we're making a very badly broken example here...
            print("(pucthread) The time is: {}Z".format(datetime.datetime.utcnow().isoformat()))
            time.sleep(1)

    def coop_printer_thread(self) -> None:
        """Also print stuff, but from a thread"""
        while self._exitcode is None:
            # This should be LOGGING but we're making a very badly broken example here...
            print("(pcthread) The time is: {}Z".format(datetime.datetime.utcnow().isoformat()))
            time.sleep(1)

    async def printer_task(self) -> None:
        """print stuff"""
        while True:
            # This should be LOGGING but we're making a very badly broken example here...
            print("(ptask) The time is: {}Z".format(datetime.datetime.utcnow().isoformat()))
            await asyncio.sleep(1)

    async def teardown(self) -> None:
        """tear down"""
        # the thread and the task should be shut down somehow here but this example is FUBAR
        await super().teardown()


# pylint: disable=W0621


@pytest.mark.skipif(
    platform.system() == "Windows",
    reason="SelectorEventloop (which we need because reasons) does not support subprocess shells",
)
@pytest.mark.asyncio
async def test_broken_service(nice_tmpdir: Path) -> None:
    """Create subprocess of the brokenservice and make sure it exits"""
    socket_uri = "ipc://" + str(Path(nice_tmpdir) / (str(uuid.uuid4()) + ".sock"))
    if platform.system() == "Windows":
        socket_uri = f"tcp://127.0.0.1:{random.randint(1337, 65000)}"  # nosec
    bork_cmd = "{exe} {name} -s {uri}".format(exe=sys.executable, uri=socket_uri, name=__file__)
    LOGGER.debug("bork_cmd={}".format(bork_cmd))

    # Create the processes
    bprocess = await asyncio.create_subprocess_shell(
        bork_cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE
    )
    # Wait max 5s for processes to finish
    bout = await asyncio.wait_for(bprocess.communicate(), 5)

    LOGGER.debug("bprocess.returncode={}".format(bprocess.returncode))
    LOGGER.debug("bout={}".format(bout))
    try:
        # Apparently the return code we get for the alarm has changed
        assert bprocess.returncode in (142, (0 - int(posixsignal.SIGALRM)))
    except AttributeError:
        # Windows does not implement all signals
        pass


@pytest.mark.skipif(
    platform.system() == "Windows",
    reason="SelectorEventloop (which we need because reasons) does not support subprocess shells",
)
@pytest.mark.asyncio
async def test_less_broken_service(nice_tmpdir: Path) -> None:
    """Create subprocess of the brokenservice and make sure it exits"""
    socket_uri = "ipc://" + str(Path(nice_tmpdir) / (str(uuid.uuid4()) + ".sock"))
    if platform.system() == "Windows":
        socket_uri = f"tcp://127.0.0.1:{random.randint(1337, 65000)}"  # nosec
    bork_cmd = "{exe} {name} -s {uri} --daemonize".format(exe=sys.executable, uri=socket_uri, name=__file__)
    LOGGER.debug("bork_cmd={}".format(bork_cmd))

    # Create the processes
    bprocess = await asyncio.create_subprocess_shell(
        bork_cmd, stdout=asyncio.subprocess.PIPE, stderr=asyncio.subprocess.PIPE
    )
    # Wait max 5s for processes to finish
    bout = await asyncio.wait_for(bprocess.communicate(), 5)

    LOGGER.debug("bprocess.returncode={}".format(bprocess.returncode))
    LOGGER.debug("bout={}".format(bout))
    assert bprocess.returncode == EXCEPTION_EXITCODE


@click.command()
@click.option("-s", "--socket_uri", help="Just to keep baseclass happy", required=True)
@click.option("--daemonize/--no-daemonize", default=False)
def broken_cli(socket_uri: str, daemonize: bool) -> None:
    """CLI entrypoint for BrokenService"""
    init_logging(logging.DEBUG)
    LOGGER.setLevel(logging.DEBUG)
    LOGGER.debug("sys.argv={}".format(sys.argv))
    with tempfile.TemporaryDirectory() as tmpdir:
        configpath = Path(tmpdir) / "config.toml"
        LOGGER.debug("writing file {}".format(configpath))
        with open(configpath, "wt", encoding="utf-8") as fpntr:
            toml.dump({"zmq": {"sub_sockets": [socket_uri]}}, fpntr)
        serv_instance = BrokenService(configpath, daemonized_uncoop=daemonize)
        exitcode = asyncio.get_event_loop().run_until_complete(serv_instance.run())
    sys.exit(exitcode)


if __name__ == "__main__":
    broken_cli()  # pylint: disable=E1120
