"""Test various heartbeat corner cases"""
import asyncio

import pytest
from datastreamcorelib.testhelpers import nice_tmpdir  # pylint: disable=W0611


# pylint: disable=W0621,W0212


@pytest.mark.asyncio
async def test_hb_socket_close(publisher):  # type: ignore
    """Test that HB exits cleanly when we close the socket"""
    assert publisher._tasks["HEARTBEAT"]
    publisher.psmgr.default_pub_socket.close()
    # The hb sleeps for 1s so we must wait longer than that for the closed socket to trigger
    await asyncio.wait_for(publisher._tasks["HEARTBEAT"], timeout=1.1)
    # TODO: check the log for the warning
    # Check that the stop will also deal with this gracefully
    await asyncio.wait_for(publisher._stop_hbtask_graceful(), timeout=0.1)
    assert "HEARTBEAT" not in publisher._tasks


@pytest.mark.asyncio
async def test_hb_method_shutdown(publisher):  # type: ignore
    """Test that HB exits cleanly when we close the socket"""
    assert publisher._tasks["HEARTBEAT"]
    # The cancel is immediate, no need for 1s sleep
    await asyncio.wait_for(publisher._stop_hbtask_graceful(), timeout=0.2)
    assert "HEARTBEAT" not in publisher._tasks


@pytest.mark.asyncio
async def test_hb_restart(publisher):  # type: ignore
    """Test that HB exits cleanly when we close the socket"""
    assert publisher._tasks["HEARTBEAT"]
    old_id = id(publisher._tasks["HEARTBEAT"])
    # The cancel is immediate, no need for 1s sleep
    await asyncio.wait_for(publisher._restart_hb_task(), timeout=0.5)
    assert id(publisher._tasks["HEARTBEAT"]) != old_id
