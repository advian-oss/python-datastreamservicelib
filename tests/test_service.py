"""Test for the service baseclasses"""
import asyncio
from pathlib import Path
import platform

import toml
import pytest
from datastreamcorelib.testhelpers import nice_tmpdir  # pylint: disable=W0611

from datastreamservicelib.service import SimpleService
import datastreamservicelib.service

# pylint: disable=W0621


@pytest.mark.asyncio
async def test_service_starts_and_quits(nice_tmpdir):  # type: ignore
    """Make sure the service starts and does not immediately die"""
    # Create empty config file
    workdir = Path(nice_tmpdir)
    configpath = workdir / Path("testconf.toml")
    ipcsock = "ipc:///" + str(workdir / Path("pub.sock"))
    if platform.system() == "Windows":
        ipcsock = "tcp://127.0.0.1:1337"
    inprocsock = "inproc://inproctest"
    with open(configpath, "wt", encoding="utf-8") as fpntr:
        toml.dump({"zmq": {"pub_sockets": [ipcsock, inprocsock]}}, fpntr)
    # Create service instance
    serv = SimpleService(configpath)
    # Force this flag to false, due to order of execution something else might have done it...
    datastreamservicelib.service.SIGNALS_HOOKED = False
    # Put it as a task in the eventloop (instead of running it as blocking via run_until_complete)
    task = asyncio.get_event_loop().create_task(serv.run())
    # Wait a moment and make sure it's still up
    await asyncio.sleep(2)
    assert datastreamservicelib.service.SIGNALS_HOOKED
    assert not task.done()
    # Make sure we have default PUBlish socket
    assert serv.psmgr.default_pub_socket
    assert not serv.psmgr.default_pub_socket.closed
    # Make sure the config got loaded
    assert "zmq" in serv.config
    assert "pub_sockets" in serv.config["zmq"]
    assert serv.config["zmq"]["pub_sockets"][0] == ipcsock
    assert serv._tasks["HEARTBEAT"]  # pylint: disable=W0212
    assert not serv._tasks["HEARTBEAT"].done()  # pylint: disable=W0212

    # Tell the service to quit and check the task is done and exitcode is correct
    serv.quit()

    async def wait_for_done() -> None:
        """Wait for the task to be done"""
        nonlocal task
        while not task.done():
            await asyncio.sleep(0.1)

    await asyncio.wait_for(wait_for_done(), timeout=1.0)
    assert task.done()
    SimpleService.clear_exit_alarm()
    assert task.result() == 0
    assert serv.psmgr.default_pub_socket.closed
    assert "HEARTBEAT" not in serv._tasks  # pylint: disable=W0212


@pytest.mark.asyncio
async def test_service_skip_hooks(nice_tmpdir):  # type: ignore
    """Make sure we can skip the signals hook"""
    # Create empty config file
    workdir = Path(nice_tmpdir)
    configpath = workdir / Path("testconf.toml")
    ipcsock = "ipc:///" + str(workdir / Path("pub.sock"))
    if platform.system() == "Windows":
        ipcsock = "tcp://127.0.0.1:1337"
    inprocsock = "inproc://inproctest"
    with open(configpath, "wt", encoding="utf-8") as fpntr:
        toml.dump({"zmq": {"pub_sockets": [ipcsock, inprocsock]}}, fpntr)
    # Create service instance
    serv = SimpleService(configpath)
    serv.disable_signals_hook = True
    # Force this flag to false, due to order of execution something else might have done it...
    datastreamservicelib.service.SIGNALS_HOOKED = False
    # Put it as a task in the eventloop (instead of running it as blocking via run_until_complete)
    task = asyncio.get_event_loop().create_task(serv.run())
    # Wait a moment and make sure it's still up
    await asyncio.sleep(1.0)
    assert not datastreamservicelib.service.SIGNALS_HOOKED
    assert not task.done()
    serv.quit()
    await asyncio.wait_for(task, timeout=1.0)
    assert task.done()
    SimpleService.clear_exit_alarm()
