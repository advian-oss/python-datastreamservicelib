"""Test the REQ/REP helpers"""
import asyncio
import uuid
import logging

import pytest
from datastreamcorelib.datamessage import PubSubDataMessage


LOGGER = logging.getLogger(__name__)


@pytest.mark.asyncio
async def test_echo_end2end(publisher, subscriber):  # type: ignore
    """Test echo via actual IPC sockets"""
    req_uri = publisher.config["zmq"]["rep_sockets"][0]
    random_str = str(uuid.uuid4())
    reply = await subscriber.send_command(req_uri, "echo", "plink", "plom", random_str, raise_on_insane=True)
    LOGGER.debug("reply={}".format(reply))
    assert reply.topic == b"reply"
    assert not reply.data["failed"]
    response = reply.data["response"]
    assert response[0] == "plink"
    assert response[-1] == random_str


@pytest.mark.asyncio
async def test_fail_end2end(publisher, subscriber):  # type: ignore
    """Test the fail method"""
    req_uri = publisher.config["zmq"]["rep_sockets"][0]
    reply = await subscriber.send_command(req_uri, "fail")
    assert reply.data["exception"]
    assert reply.data["reason"].startswith("RuntimeError")
    assert "you asked for failure" in reply.data["reason"]

    # Test the automatic error raise if we have trouble
    with pytest.raises(RuntimeError):
        await subscriber.send_command(req_uri, "fail", raise_on_insane=True)


@pytest.mark.asyncio
async def test_blocking_error(subscriber):  # type: ignore
    """Test that trying to call the blocking method raises error"""
    with pytest.raises(TypeError):
        subscriber._do_reqrep_blocking(b"dummy", PubSubDataMessage(b"dummy"))  # pylint: disable=W0212


@pytest.mark.asyncio
async def test_task_socket_close(publisher):  # type: ignore
    """Test closing of the socket"""
    task = publisher._tasks["DEFAULT_REP"]  # pylint: disable=W0212
    assert not task.done()
    publisher.psmgr.sockethandler.close_all_sockets()
    await asyncio.wait_for(task, timeout=0.1)


@pytest.mark.asyncio
async def test_task_cancel(publisher):  # type: ignore
    """Test that cancel of task is handled gracefully"""
    task = publisher._tasks["DEFAULT_REP"]  # pylint: disable=W0212
    assert not task.done()
    task.cancel()
    await asyncio.wait_for(task, timeout=0.1)
