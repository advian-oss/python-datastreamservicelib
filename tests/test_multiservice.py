"""Test multiple services running concurrently in same eventloop"""
from typing import List, Any, cast
import asyncio
from pathlib import Path
import logging
import platform
import random

import toml
import pytest
import zmq
import zmq.asyncio
from libadvian.binpackers import ensure_str
from datastreamcorelib.logging import init_logging
from datastreamcorelib.abstract import ZMQSocketType
from datastreamcorelib.pubsub import Subscription
from datastreamcorelib.datamessage import PubSubDataMessage
from datastreamcorelib.testhelpers import nice_tmpdir  # pylint: disable=W0611


from datastreamservicelib.zmqwrappers import Socket
from datastreamservicelib.service import SimpleService
from datastreamservicelib.console import CollectingSubscriber, Publisher

init_logging(logging.DEBUG)
LOGGER = logging.getLogger(__name__)
#  pylint: disable=W0621


async def wait_for_messages_raw(socketuri: bytes, topic: bytes = b"HEARTBEAT", timeout: float = 1.2) -> None:
    """wait for message in (by default) heartbeat topic"""
    zmqsocket = zmq.asyncio.Context.instance().socket(zmq.SUB)  # pylint: disable=E1101
    zmqsocket.connect(ensure_str(socketuri))
    zmqsocket.subscribe(topic)

    async def get_messages() -> List[Any]:
        """wrap socket call so we can timeout"""
        nonlocal timeout
        LOGGER.debug("POLLing on {} ({!r}/{!r})".format(zmqsocket, socketuri, topic))
        pollresult = await zmqsocket.poll(timeout=int(timeout * 1000))
        if pollresult:
            return cast(List[Any], await zmqsocket.recv_multipart())
        return []

    try:
        msgparts = await asyncio.wait_for(get_messages(), timeout + 0.1)
        if not msgparts:
            raise TimeoutError("Did not get messages")
        LOGGER.debug("Got msgs on {!r}".format(socketuri))
    finally:
        zmqsocket.close()


async def wait_for_messages_wrapped(socketuri: bytes, topic: bytes = b"HEARTBEAT", timeout: float = 1.2) -> None:
    """wait for message in (by default) heartbeat topic"""
    zmqsocket = Socket(ZMQSocketType.SUB)
    zmqsocket.connect(ensure_str(socketuri))
    zmqsocket.subscribe(topic)

    async def get_messages() -> List[Any]:
        """wrap socket call so we can timeout"""
        nonlocal timeout
        LOGGER.debug("POLLing on {} ({!r}/{!r})".format(zmqsocket, socketuri, topic))
        pollresult = await zmqsocket.poll(timeout=int(timeout * 1000))
        if pollresult:
            return cast(List[Any], await zmqsocket.recv_multipart())
        return []

    try:
        msgparts = await asyncio.wait_for(get_messages(), timeout + 0.1)
        if not msgparts:
            raise TimeoutError("Did not get messages")
        LOGGER.debug("Got msgs on {!r}".format(socketuri))
    finally:
        zmqsocket.close()


async def publish_messages_raw(socketuri: bytes, topic: bytes = b"HEARTBEAT", cnt: int = 3) -> None:
    """Send messages on raw socket, just to check it at least works"""
    zmqsocket = zmq.asyncio.Context.instance().socket(zmq.PUB)  # pylint: disable=E1101
    zmqsocket.bind(ensure_str(socketuri))
    try:
        for _ in range(cnt):
            LOGGER.debug("Raw sending on {!r}/{!r}".format(socketuri, topic))
            msg = PubSubDataMessage(topic)
            await zmqsocket.send_multipart(msg.zmq_encode())
            await asyncio.sleep(0.1)
    finally:
        zmqsocket.close()


async def publish_messages_wrapped(socketuri: bytes, topic: bytes = b"HEARTBEAT", cnt: int = 3) -> None:
    """Send messages on wrapped socket, just to check it at least works"""
    zmqsocket = Socket(ZMQSocketType.PUB)
    zmqsocket.bind(socketuri)
    try:
        for _ in range(cnt):
            LOGGER.debug("Wrapped sending on {!r}/{!r}".format(socketuri, topic))
            msg = PubSubDataMessage(topic)
            await zmqsocket.send_multipart(msg.zmq_encode())
            await asyncio.sleep(0.1)
    finally:
        zmqsocket.close()


@pytest.mark.parametrize("amount", [2, 5, 10])
@pytest.mark.asyncio
async def test_n_raw2raw(nice_tmpdir, event_loop, amount):  # type: ignore
    """Test N raw socket tasks sending stuff to raw sockets"""
    workdir = Path(nice_tmpdir)
    tasks: List["asyncio.Task[Any]"] = []
    baseport = random.randint(1337, 65000)  # nosec
    for idx in range(amount):
        topic = "testtopic_{}".format(idx).encode("utf-8")
        ipcsock = str("ipc:///" + str(workdir / "pub_{}.sock".format(idx))).encode("utf-8")
        if platform.system() == "Windows":
            ipcsock = f"tcp://127.0.0.1:{baseport+idx}".encode("utf-8")
        tasks.append(event_loop.create_task(wait_for_messages_raw(ipcsock, topic, timeout=2.0)))
        tasks.append(event_loop.create_task(publish_messages_raw(ipcsock, topic, cnt=10)))
    await asyncio.wait_for(asyncio.gather(*tasks), timeout=2.5)


@pytest.mark.parametrize("amount", [2, 5, 10])
@pytest.mark.asyncio
async def test_n_wrapped2raw(nice_tmpdir, event_loop, amount):  # type: ignore
    """Test N wraooed socket tasks sending stuff to raw sockets"""
    workdir = Path(nice_tmpdir)
    tasks: List["asyncio.Task[Any]"] = []
    baseport = random.randint(1337, 65000)  # nosec
    for idx in range(amount):
        topic = "testtopic_{}".format(idx).encode("utf-8")
        ipcsock = str("ipc:///" + str(workdir / "pub_{}.sock".format(idx))).encode("utf-8")
        if platform.system() == "Windows":
            ipcsock = f"tcp://127.0.0.1:{baseport+idx}".encode("utf-8")
        tasks.append(event_loop.create_task(wait_for_messages_raw(ipcsock, topic, timeout=2.0)))
        tasks.append(event_loop.create_task(publish_messages_wrapped(ipcsock, topic, cnt=10)))
    await asyncio.wait_for(asyncio.gather(*tasks), timeout=2.0)


@pytest.mark.parametrize("amount", [2, 5, 10])
@pytest.mark.asyncio
async def test_n_wrapped2wrapped(nice_tmpdir, event_loop, amount):  # type: ignore
    """Test N wrapped socket tasks sending stuff to wrapped sockets"""
    workdir = Path(nice_tmpdir)
    tasks: List["asyncio.Task[Any]"] = []
    baseport = random.randint(1337, 65000)  # nosec
    for idx in range(amount):
        topic = "testtopic_{}".format(idx).encode("utf-8")
        ipcsock = str("ipc:///" + str(workdir / "pub_{}.sock".format(idx))).encode("utf-8")
        if platform.system() == "Windows":
            ipcsock = f"tcp://127.0.0.1:{baseport+idx}".encode("utf-8")
        tasks.append(event_loop.create_task(wait_for_messages_wrapped(ipcsock, topic, timeout=2.0)))
        tasks.append(event_loop.create_task(publish_messages_wrapped(ipcsock, topic, cnt=10)))
    await asyncio.wait_for(asyncio.gather(*tasks), timeout=2.0)


@pytest.mark.parametrize("amount", [2, 5, 10])
@pytest.mark.asyncio
async def test_n_raw2wrapped(nice_tmpdir, event_loop, amount):  # type: ignore
    """Test N raw socket tasks sending stuff to wrapped sockets"""
    workdir = Path(nice_tmpdir)
    tasks: List["asyncio.Task[Any]"] = []
    baseport = random.randint(1337, 65000)  # nosec
    for idx in range(amount):
        topic = "testtopic_{}".format(idx).encode("utf-8")
        ipcsock = str("ipc:///" + str(workdir / "pub_{}.sock".format(idx))).encode("utf-8")
        if platform.system() == "Windows":
            ipcsock = f"tcp://127.0.0.1:{baseport+idx}".encode("utf-8")
        tasks.append(event_loop.create_task(wait_for_messages_wrapped(ipcsock, topic, timeout=2.0)))
        tasks.append(event_loop.create_task(publish_messages_raw(ipcsock, topic, cnt=10)))
    await asyncio.wait_for(asyncio.gather(*tasks), timeout=2.0)


@pytest.mark.parametrize("amount", [2, 5, 10])
@pytest.mark.asyncio
async def test_n_raw2subcriber(nice_tmpdir, event_loop, amount):  # type: ignore
    """Test N raw sockets sending stuff to subsciber"""
    workdir = Path(nice_tmpdir)
    tasks: List["asyncio.Task[Any]"] = []

    sub_configpath = workdir / "subscriber.toml"
    with open(sub_configpath, "wt", encoding="utf-8") as fpntr:
        toml.dump({"zmq": {"sub_sockets": ["inproc://dummysock"], "pub_sockets": ["inproc://dummysock2"]}}, fpntr)
    subscriber = CollectingSubscriber(topic=b"dummytopic", configpath=sub_configpath)
    subsriber_task = event_loop.create_task(subscriber.run())
    await wait_for_messages_raw(b"inproc://dummysock2")
    subscriptions: List[Subscription] = []

    # Create service instances with their own socket paths
    baseport = random.randint(1337, 65000)  # nosec
    for idx in range(amount):
        topic = "testtopic_{}".format(idx).encode("utf-8")
        ipcsock = str("ipc:///" + str(workdir / "pub_{}.sock".format(idx))).encode("utf-8")
        if platform.system() == "Windows":
            ipcsock = f"tcp://127.0.0.1:{baseport+idx}".encode("utf-8")
        sub = Subscription(ipcsock, topic, subscriber.success_callback, decoder_class=PubSubDataMessage)
        LOGGER.debug("Subscribing {!r}/{!r}".format(ipcsock, topic))
        subscriber.psmgr.subscribe_async(sub)
        tasks.append(event_loop.create_task(publish_messages_raw(ipcsock, topic, cnt=10)))
        subscriptions.append(sub)

    await asyncio.wait_for(asyncio.gather(*tasks), timeout=2.0)

    # Check that the subscriber got messages
    for sub in subscriptions:
        assert sub.trackingid in subscriber.messages_by_sub
        assert len(subscriber.messages_by_sub[sub.trackingid]) > 1

    subscriber.quit(0)
    await subsriber_task
    # Finally clear the alarm
    SimpleService.clear_exit_alarm()


@pytest.mark.parametrize("amount", [1, 2, 5, 10])
@pytest.mark.asyncio
async def test_n_publisher2raw(nice_tmpdir, event_loop, amount):  # type: ignore
    """Test N publishers with raw receivers"""
    workdir = Path(nice_tmpdir)
    publisher_instances: List[SimpleService] = []
    pub_configfiles: List[Path] = []
    publisher_tasks: List["asyncio.Task[Any]"] = []
    listener_tasks: List["asyncio.Task[Any]"] = []

    # Create service instances with their own socket paths
    baseport = random.randint(1337, 65000)  # nosec
    for idx in range(amount):
        topic = "testtopic_{}".format(idx).encode("utf-8")
        ipcsock = str("ipc:///" + str(workdir / "pub_{}.sock".format(idx)))
        if platform.system() == "Windows":
            ipcsock = f"tcp://127.0.0.1:{baseport+idx}"
        configpath = workdir / "publisher_{}.toml".format(idx)
        with open(configpath, "wt", encoding="utf-8") as fpntr:
            toml.dump(
                {
                    "zmq": {
                        "pub_sockets": [
                            ipcsock,
                        ]
                    }
                },
                fpntr,
            )
        publisher_instance = Publisher(topic=topic, configpath=configpath, send_count=10)
        publisher_instances.append(publisher_instance)
        pub_configfiles.append(configpath)
        publisher_tasks.append(event_loop.create_task(publisher_instance.run()))
        listener_tasks.append(event_loop.create_task(wait_for_messages_raw(ipcsock.encode("utf-8"), topic)))

    await asyncio.wait_for(asyncio.gather(*listener_tasks), timeout=2.0)
    await asyncio.wait_for(asyncio.gather(*publisher_tasks), timeout=2.0)
    SimpleService.clear_exit_alarm()


@pytest.mark.parametrize("amount", [1, 2, 5, 10])
@pytest.mark.asyncio
async def test_n_publishers2subscriber(nice_tmpdir, event_loop, amount):  # type: ignore # pylint: disable=R0914
    """Test N publishers in same event loop"""
    workdir = Path(nice_tmpdir)
    publisher_instances: List[SimpleService] = []
    pub_configfiles: List[Path] = []

    sub_configpath = workdir / "subscriber.toml"
    with open(sub_configpath, "wt", encoding="utf-8") as fpntr:
        toml.dump({"zmq": {"sub_sockets": ["inproc://dummysock"], "pub_sockets": ["inproc://dummysock2"]}}, fpntr)
    subscriber = CollectingSubscriber(topic=b"dummytopic", configpath=sub_configpath)
    subsriber_task = event_loop.create_task(subscriber.run())
    await wait_for_messages_raw(b"inproc://dummysock2")
    subscriptions: List[Subscription] = []

    # Create service instances with their own socket paths
    baseport = random.randint(1337, 65000)  # nosec
    for idx in range(amount):
        topic = "testtopic_{}".format(idx).encode("utf-8")
        ipcsock = str("ipc:///" + str(workdir / "pub_{}.sock".format(idx)))
        if platform.system() == "Windows":
            ipcsock = f"tcp://127.0.0.1:{baseport+idx}"
        configpath = workdir / "publisher_{}.toml".format(idx)
        with open(configpath, "wt", encoding="utf-8") as fpntr:
            toml.dump(
                {
                    "zmq": {
                        "pub_sockets": [
                            ipcsock,
                        ]
                    }
                },
                fpntr,
            )
        publisher_instances.append(Publisher(topic=topic, configpath=configpath, send_count=10))
        pub_configfiles.append(configpath)
        sub = Subscription(ipcsock, topic, subscriber.success_callback, decoder_class=PubSubDataMessage)
        LOGGER.debug("Subscribing {}/{!r}".format(ipcsock, topic))
        subscriber.psmgr.subscribe_async(sub)
        subscriptions.append(sub)

    publisher_tasks: List["asyncio.Task[Any]"] = []
    for instance in publisher_instances:
        publisher_tasks.append(event_loop.create_task(instance.run()))
        await asyncio.sleep(0.01)
        assert instance._tasks["HEARTBEAT"]  # pylint: disable=W0212
        assert instance.psmgr
        assert instance.psmgr.default_pub_socket

    # Wait for the messages to be sent
    await asyncio.wait_for(asyncio.gather(*publisher_tasks), timeout=1.5)
    SimpleService.clear_exit_alarm()

    # Check that the subscriber got messages
    for sub in subscriptions:
        assert sub in subscriber.psmgr.subscriptions
        assert sub.trackingid in subscriber.messages_by_sub
        assert subscriber.messages_by_sub[sub.trackingid]

    # Wait for tasks to finish
    subscriber.quit(0)
    await subsriber_task
    # Finally clear the alarm
    SimpleService.clear_exit_alarm()
