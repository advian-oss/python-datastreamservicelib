"""Test the ZMQ wrappers"""
from typing import cast, Optional, List, Any
import asyncio
import logging
import pytest

from datastreamcorelib.logging import init_logging
from datastreamcorelib.abstract import ZMQSocketType, BaseSocketHandler, ZMQSocketDescription
from datastreamcorelib.pubsub import Subscription, PubSubMessage
from datastreamservicelib.zmqwrappers import Socket, SocketHandler, pubsubmanager_factory

from .conftest import CBTestException

LOGGER = logging.getLogger(__name__)


def simple_success_callback(sub: Subscription, msg: PubSubMessage) -> None:  # pylint: disable=W0613
    """simple func for subscription callback"""
    LOGGER.debug("got msg {}".format(msg))


def test_socket_instantiation() -> None:
    """Make sure we can init all socket types defined byt datastreamcorelib."""
    for stype in ZMQSocketType:
        zmqsocket = Socket(stype)
        assert zmqsocket._raw_socket  # pylint: disable=W0212
        assert zmqsocket.socket_type == stype


def test_socket_wraps_connect_subscribe_disconnect_close() -> None:
    """Make sure the raw socket (dis)connect method is available in the wrapped one."""
    zmqsocket = Socket(ZMQSocketType.SUB)
    zmqsocket.connect("inproc://connectwraptest")
    zmqsocket.subscribe(b"foobar")
    zmqsocket.disconnect("inproc://connectwraptest")
    zmqsocket.close()


def test_socket_hashes() -> None:
    """Make sure we get hashes that are constructed correctly"""
    zmqsocket = Socket(ZMQSocketType.SUB)
    raw_hash = zmqsocket._raw_socket.__hash__()  # pylint: disable=W0212,C2801
    wrapped_hash = zmqsocket.__hash__()  # pylint: disable=C2801
    assert wrapped_hash == raw_hash + zmqsocket.socket_type.value


def test_socket_dir() -> None:
    """Test the __dir__ method wrapper"""
    zmqsocket = Socket(ZMQSocketType.SUB)
    retlst = dir(zmqsocket)
    assert "socket_type" in retlst
    assert "__post_init__" in retlst
    assert "monitor" in retlst


def test_sockethandler_singleton() -> None:
    """Test the singleton method and inheritance"""
    handler = SocketHandler.instance()
    assert isinstance(handler, SocketHandler)
    assert isinstance(handler, BaseSocketHandler)
    handler2 = SocketHandler.instance()
    assert handler == handler2


def test_sockethandler_gettter() -> None:
    """Make sure we can get sockets via sockethandler"""
    socketdesc1 = ZMQSocketDescription([b"inproc://connectwraptest"], ZMQSocketType.PUB)
    handler = SocketHandler.instance()
    socket1 = handler.get_socket(socketdesc1)
    assert isinstance(socket1, Socket)
    socket1.close()


def test_sockethandler_subscribe() -> None:
    """Make sure we can add subscroptions via sockethandler"""
    socketdesc1 = ZMQSocketDescription([b"inproc://connectwraptest"], ZMQSocketType.SUB)
    handler = SocketHandler.instance()
    socket1 = handler.get_socket(socketdesc1)
    assert isinstance(socket1, Socket)
    # Make sure we can subscribe to a topic
    socket1.subscribe(b"hdl_test_topic")
    # NOTE: we cannot list the active topic filters so can't check further without actually sending messages
    socket1.close()


def test_sockethandler_close_all() -> None:
    """Test the close_all_sockets method"""
    socketdesc1 = ZMQSocketDescription([b"inproc://connectwraptest"], ZMQSocketType.SUB)
    socketdesc2 = ZMQSocketDescription([b"inproc://connectwraptest2"], ZMQSocketType.PUB)
    handler = SocketHandler.instance()
    socket1 = handler.get_socket(socketdesc1)
    socket2 = handler.get_socket(socketdesc2)
    handler.close_all_sockets()
    assert socket1.closed
    assert socket2.closed
    # Also make sure we get a freshly opened one when fetching a closed socket
    socket3 = handler.get_socket(socketdesc1)
    assert not socket3.closed
    socket3.close()


@pytest.mark.asyncio
async def test_manager_subscribe() -> None:
    """Make sure our subscriptionmanager works"""
    socketdesc1 = ZMQSocketDescription([b"inproc://pubsubtest"], ZMQSocketType.SUB)
    socketdesc2 = ZMQSocketDescription([b"inproc://pubsubtest"], ZMQSocketType.PUB)
    assert socketdesc1.__hash__() != socketdesc2.__hash__()  # pylint: disable=C2801
    submgr = pubsubmanager_factory()

    # Create publisher (it will auto-bind)
    socket2 = cast(Socket, submgr.sockethandler.get_socket(socketdesc2))
    # TODO: figure out a better way to wait for socket to stabilize
    await asyncio.sleep(0.1)

    # Subscribe
    subdesc = Subscription(socketdesc1.socketuris, "dispatch_test_1", simple_success_callback)
    submgr.subscribe(subdesc)
    # pylint: disable=W0212
    assert submgr._socket_reader_tasks
    sock = cast(Socket, submgr.sockethandler.get_socket(socketdesc1))
    assert sock in submgr.sockethandler.open_sockets
    assert sock in submgr._socket_reader_tasks
    assert not submgr._socket_reader_tasks[sock].done()

    # Send a message
    await socket2.send_multipart([b"dispatch_test_1", b"something"])
    # TODO: figure out a better way to wait message to be handled
    await asyncio.sleep(0.1)

    sock.close()
    socket2.close()


@pytest.mark.asyncio
async def test_manager_publish_async() -> None:
    """Test that published messages are sent without problems"""
    sdesc = ZMQSocketDescription([b"inproc://pubsubtest"], ZMQSocketType.SUB)
    sdesc_pub = ZMQSocketDescription([b"inproc://pubsubtest"], ZMQSocketType.PUB)
    mgr = pubsubmanager_factory()
    msg = PubSubMessage.zmq_decode([b"signaturetest", b"plim", b"plom"])

    received_msgs: List[PubSubMessage] = []

    async def success_callback(sub: Subscription, msg: PubSubMessage) -> None:  # pylint: disable=W0613
        nonlocal received_msgs
        _ = sub
        received_msgs.append(msg)

    async def wait_for_n_msgs(amount: int) -> None:
        nonlocal received_msgs
        while len(received_msgs) < amount:
            await asyncio.sleep(0.1)

    subdesc = Subscription(sdesc.socketuris, msg.topic, success_callback)
    mgr.subscribe_async(subdesc)

    # URI
    await mgr.publish_async(msg, sdesc.socketuris)
    await asyncio.wait_for(wait_for_n_msgs(1), timeout=0.5)

    # Explicit socket
    sock = mgr.sockethandler.get_socket(sdesc_pub)
    await mgr.publish_async(msg, sock)
    await asyncio.wait_for(wait_for_n_msgs(2), timeout=0.5)

    # Set default socket and make sure it works
    mgr.default_pub_socket = sock
    await mgr.publish_async(msg)
    await asyncio.wait_for(wait_for_n_msgs(3), timeout=0.5)

    mgr.sockethandler.close_all_sockets()


@pytest.mark.asyncio
async def test_manager_publish_sync() -> None:
    """Test that published messages are sent without problems"""
    sdesc = ZMQSocketDescription([b"inproc://pubsubtest"], ZMQSocketType.SUB)
    sdesc_pub = ZMQSocketDescription([b"inproc://pubsubtest"], ZMQSocketType.PUB)
    mgr = pubsubmanager_factory()
    msg = PubSubMessage.zmq_decode([b"signaturetest", b"plim", b"plom"])

    received_msgs: List[PubSubMessage] = []

    def success_callback(sub: Subscription, msg: PubSubMessage) -> None:  # pylint: disable=W0613
        nonlocal received_msgs
        _ = sub
        received_msgs.append(msg)

    async def wait_for_n_msgs(amount: int) -> None:
        nonlocal received_msgs
        while len(received_msgs) < amount:
            await asyncio.sleep(0.1)

    subdesc = Subscription(sdesc.socketuris, msg.topic, success_callback)
    mgr.subscribe(subdesc)

    # URI
    mgr.publish(msg, sdesc.socketuris)
    await asyncio.wait_for(wait_for_n_msgs(1), timeout=0.5)

    # Explicit socket
    sock = mgr.sockethandler.get_socket(sdesc_pub)
    mgr.publish(msg, sock)
    await asyncio.wait_for(wait_for_n_msgs(2), timeout=0.5)

    # Set default socket and make sure it works
    mgr.default_pub_socket = sock
    mgr.publish(msg)
    await asyncio.wait_for(wait_for_n_msgs(3), timeout=0.5)

    # TODO: Capture logs and make sure we got the warning
    mgr.sockethandler.close_all_sockets()


@pytest.mark.asyncio
async def test_req_rep() -> None:
    """Test REQ/REP socket"""
    sdesc_req = ZMQSocketDescription([b"inproc://reqreptest"], ZMQSocketType.REQ)
    sdesc_rep = ZMQSocketDescription([b"inproc://reqreptest"], ZMQSocketType.REP)
    handler = SocketHandler.instance()

    async def responder() -> None:
        """Respond to 5 messages with the same"""
        nonlocal sdesc_rep
        sock = cast(Socket, handler.get_socket(sdesc_rep))
        for _ in range(5):
            msgparts = await asyncio.wait_for(sock.recv_multipart(), 0.5)
            await sock.send_multipart(msgparts)
        sock.close()

    resp_task = asyncio.get_event_loop().create_task(responder())

    last_response: Optional[List[Any]] = None

    async def requester() -> None:
        """Send 5 messages and wait for results"""
        nonlocal sdesc_req, last_response
        sock = cast(Socket, handler.get_socket(sdesc_req))
        for cnt in range(5):
            await sock.send_multipart([b"PING", bytes([cnt])])
            response = await asyncio.wait_for(sock.recv_multipart(), 0.5)
            assert response == [b"PING", bytes([cnt])]
            last_response = response
        sock.close()

    req_task = asyncio.get_event_loop().create_task(requester())

    await asyncio.gather(
        asyncio.wait_for(resp_task, 2),
        asyncio.wait_for(req_task, 2),
    )
    assert last_response == [b"PING", bytes([4])]


@pytest.mark.asyncio
async def test_manager_publish_async_cbexception(capsys) -> None:  # type: ignore
    """Test that published messages are sent without problems"""
    init_logging(logging.DEBUG)  # we must explicitly init again due to capsys
    sdesc = ZMQSocketDescription([b"inproc://pubsubtest_exc"], ZMQSocketType.SUB)
    sdesc_pub = ZMQSocketDescription([b"inproc://pubsubtest_exc"], ZMQSocketType.PUB)
    mgr = pubsubmanager_factory()
    mgr.default_pub_socket = mgr.sockethandler.get_socket(sdesc_pub)

    msg = PubSubMessage.zmq_decode([b"signaturetest", b"plim", b"plom"])

    async def success_callback_exc(sub: Subscription, msg: PubSubMessage) -> None:  # pylint: disable=W0613
        _, _ = sub, msg
        raise CBTestException("testing exception handling")

    subdesc = Subscription(sdesc.socketuris, msg.topic, success_callback_exc)
    mgr.subscribe_async(subdesc)

    await mgr.publish_async(msg)
    await asyncio.sleep(0.1)
    (_, stderr) = capsys.readouterr()
    assert "Exception in callback PubSubManager.subscribe." in stderr
    assert "tests.conftest.CBTestException: testing exception handling" in stderr

    mgr.sockethandler.close_all_sockets()
