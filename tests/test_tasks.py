"""Test the task wrapper"""
from typing import cast, Optional, Any, List
import asyncio
import uuid

import pytest
from datastreamservicelib.service import SimpleService
from datastreamservicelib.compat import cast_task


async def sleeper(amount: float, retval: Optional[Any] = None) -> Optional[Any]:
    """sleep for X"""
    await asyncio.sleep(amount)
    return retval


async def graceful_sleeper(amount: float, retval: Optional[Any] = None) -> Optional[Any]:
    """sleep for X but handle cancel cleanly"""
    try:
        await asyncio.sleep(amount)
    except asyncio.CancelledError:
        pass
    finally:
        return retval  # pylint: disable=W0150


async def exceptional_task() -> None:
    """raises an exception"""
    raise RuntimeError("deal with this!")


@pytest.mark.asyncio
async def test_task_name(publisher):  # type: ignore
    """Test that we can create a task with name"""
    # pylint: disable=W0212
    serv = cast(SimpleService, publisher)

    name = str(uuid.uuid4())
    task = serv.create_task(sleeper(0.1), name=name)
    task = cast_task(task)
    assert task
    assert not task.done()
    assert task.get_name() == name
    assert name in serv._tasks
    await task
    assert name not in serv._tasks
    del task

    name = str(uuid.uuid4())
    task1 = serv.create_task(sleeper(0.1), name=name)
    assert task1
    assert not task1.done()
    with pytest.raises(ValueError):
        _ = serv.create_task(sleeper(0.1), name=name)
    await task1
    assert name not in serv._tasks


@pytest.mark.asyncio
async def test_task_stop(publisher):  # type: ignore
    """Test stopping long running tasks gracefully"""
    serv = cast(SimpleService, publisher)

    # Named stop (goes actually to instance)
    task = serv.create_task(graceful_sleeper(10.0), name="long_sleep_test")
    assert task
    await asyncio.sleep(0.1)
    assert not task.done()
    await asyncio.wait_for(serv.stop_named_task_graceful("long_sleep_test"), timeout=0.5)
    assert task.done()
    del task

    # Instance stop
    task = serv.create_task(graceful_sleeper(10.0))
    assert task
    await asyncio.sleep(0.1)
    assert not task.done()
    await asyncio.wait_for(serv.stop_task_graceful(task), timeout=0.5)
    assert task.done()
    del task


@pytest.mark.asyncio
async def test_task_stop_result(publisher):  # type: ignore
    """Test stopping long running tasks gracefully"""
    serv = cast(SimpleService, publisher)
    taskname = str(uuid.uuid4())
    teststr = str(uuid.uuid4())
    task = serv.create_task(graceful_sleeper(10.0, teststr), name=taskname)
    assert task
    await asyncio.sleep(0.1)
    assert not task.done()

    result = await asyncio.wait_for(serv.stop_named_task_graceful(taskname), timeout=0.5)
    assert task.done()
    assert result == teststr


@pytest.mark.asyncio
async def test_task_exception(publisher):  # type: ignore
    """Test task that raises exception"""
    serv = cast(SimpleService, publisher)
    taskname = str(uuid.uuid4())
    task = serv.create_task(exceptional_task(), name=taskname)
    assert task
    await asyncio.sleep(0.1)
    assert task.done()
    with pytest.raises(RuntimeError):
        await task


@pytest.mark.asyncio
async def test_task_stop_cancellederror(publisher):  # type: ignore
    """Test stopping long running tasks gracefully"""
    serv = cast(SimpleService, publisher)
    taskname = str(uuid.uuid4())
    task = serv.create_task(sleeper(10.0), name=taskname)
    assert task
    await asyncio.sleep(0.1)
    assert not task.done()

    await asyncio.wait_for(serv.stop_named_task_graceful(taskname), timeout=0.5)
    assert task.done()
    assert taskname not in serv._tasks  # pylint: disable=W0212


@pytest.mark.asyncio
async def test_task_stop_lingering_misbehaved(publisher):  # type: ignore
    """Test stopping lingering tasks"""
    # pylint: disable=W0212
    amount = 10
    serv = cast(SimpleService, publisher)
    tasks: List["asyncio.Task[Any]"] = []
    for _ in range(amount):
        teststr = str(uuid.uuid4())
        tasks.append(serv.create_task(sleeper(10.0, teststr)))
    await asyncio.sleep(0.1)
    for task in tasks:
        assert not task.done()
        assert task in serv._tasks.values()

    await serv.stop_lingering_tasks()
    for task in tasks:
        assert task.done()
        assert task not in serv._tasks.values()


@pytest.mark.asyncio
async def test_task_stop_lingering(publisher):  # type: ignore
    """Test stopping lingering tasks"""
    # pylint: disable=W0212
    amount = 10
    serv = cast(SimpleService, publisher)
    tasks: List["asyncio.Task[Any]"] = []
    for _ in range(amount):
        teststr = str(uuid.uuid4())
        tasks.append(serv.create_task(graceful_sleeper(10.0, teststr)))
    await asyncio.sleep(0.1)
    for task in tasks:
        assert not task.done()
        assert task in serv._tasks.values()

    await serv.stop_lingering_tasks()
    for task in tasks:
        assert task.done()
        assert task not in serv._tasks.values()
