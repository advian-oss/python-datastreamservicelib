"""Test config parsing with complex and weird documents"""
import os.path
from pathlib import Path
import platform

import pytest

from datastreamservicelib.service import SimpleService


@pytest.mark.skipif(platform.system() == "Windows", reason="Windows does not support ipc sockets which the config uses")
@pytest.mark.asyncio
async def test_weird_config_loading():  # type: ignore
    """Make sure the weird config is parsed correctly"""
    configpath = Path(os.path.dirname(__file__)) / "broken.toml"
    assert configpath.exists()
    serv = SimpleService(configpath)
    serv.reload()
    try:
        config = serv.config
        assert "internal_zmq_uri" in config["gnss"]["source"]["kwargs"]
        # recast to dict to expose the problem
        subconf = dict(config["gnss"]["source"]["kwargs"])
        assert "internal_zmq_uri" in subconf
    finally:
        await serv.teardown()
