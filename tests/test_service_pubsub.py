"""Test pubsub using services"""
import asyncio
import uuid
from pathlib import Path
import logging
import platform

import pytest
from libadvian.testhelpers import nice_tmpdir  # pylint: disable=W0611


LOGGER = logging.getLogger(__name__)

#  pylint: disable=W0621


@pytest.mark.skipif(
    platform.system() == "Windows",
    reason="SelectorEventloop (which we need because reasons) does not support subprocess shells",
)
@pytest.mark.asyncio
async def test_pubsub_cli(nice_tmpdir: Path) -> None:
    """Test the pub/sub cli scripts"""
    socket_uri = "ipc://" + str(Path(nice_tmpdir) / (str(uuid.uuid4()) + ".sock"))
    if platform.system() == "Windows":
        socket_uri = "tcp://127.0.0.1:1337"
    topic = str(uuid.uuid4())
    sub_cmd = "testsubscriber -s {} -t {} -c 10".format(socket_uri, topic)
    pub_cmd = "testpublisher -s {} -t {} -c 15".format(socket_uri, topic)
    LOGGER.debug("sub_cmd={}".format(sub_cmd))
    LOGGER.debug("pub_cmd={}".format(pub_cmd))

    # Create the processes
    sprocess, pprocess = await asyncio.gather(
        asyncio.create_subprocess_shell(
            sub_cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        ),
        asyncio.create_subprocess_shell(
            pub_cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        ),
    )
    # Wait max 5s for processes to finish
    sout, pout = await asyncio.gather(
        asyncio.wait_for(sprocess.communicate(), 5), asyncio.wait_for(pprocess.communicate(), 5)
    )

    LOGGER.debug("sprocess.returncode={}".format(sprocess.returncode))
    LOGGER.debug("sout={}".format(sout))
    LOGGER.debug("pprocess.returncode={}".format(pprocess.returncode))
    LOGGER.debug("pout={}".format(pout))

    # Make sure they exited cleanly
    assert sprocess.returncode == 0
    assert pprocess.returncode == 0


@pytest.mark.skipif(
    platform.system() == "Windows",
    reason="SelectorEventloop (which we need because reasons) does not support subprocess shells",
)
@pytest.mark.asyncio
async def test_pubsub_img_cli(nice_tmpdir: Path) -> None:
    """Test the pub/sub cli scripts"""
    socket_uri = "ipc://" + str(Path(nice_tmpdir) / (str(uuid.uuid4()) + ".sock"))
    if platform.system() == "Windows":
        socket_uri = "tcp://127.0.0.1:1337"
    topic = str(uuid.uuid4())
    sub_cmd = "testsubscriber -s {} -t {} -i -c 10".format(socket_uri, topic)
    pub_cmd = "testpublisher -s {} -t {} -i -c 15".format(socket_uri, topic)
    LOGGER.debug("sub_cmd={}".format(sub_cmd))
    LOGGER.debug("pub_cmd={}".format(pub_cmd))

    # Create the processes
    sprocess, pprocess = await asyncio.gather(
        asyncio.create_subprocess_shell(
            sub_cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        ),
        asyncio.create_subprocess_shell(
            pub_cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE,
        ),
    )
    # Wait max 5s for processes to finish
    sout, pout = await asyncio.gather(
        asyncio.wait_for(sprocess.communicate(), 5), asyncio.wait_for(pprocess.communicate(), 5)
    )

    LOGGER.debug("sprocess.returncode={}".format(sprocess.returncode))
    LOGGER.debug("sout={}".format(sout))
    LOGGER.debug("pprocess.returncode={}".format(pprocess.returncode))
    LOGGER.debug("pout={}".format(pout))

    # Make sure they exited cleanly
    assert sprocess.returncode == 0
    assert pprocess.returncode == 0
